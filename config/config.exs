# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :tesla, :adapter, Tesla.Adapter.Httpc

config :dlt_worker, :target, DLTWorker.Target.HTTP

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
if File.exists?("./config/#{Mix.env()}.exs") do
  import_config "#{Mix.env()}.exs"
end

# Import secret config specific for current environment.
# These configs are not tracked by VCS, so there may be no such file.
if File.exists?("./config/#{Mix.env()}.secret.exs") do
  import_config "#{Mix.env()}.secret.exs"
end
