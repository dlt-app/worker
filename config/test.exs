import Config

config :tesla, adapter: Tesla.Mock

config :dlt_worker, :target, DLTWorker.Target.Mock

# Print only warnings and errors during test
config :logger, level: :warn
