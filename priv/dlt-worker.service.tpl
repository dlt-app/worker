; This service is not intended to run in production: it has security issues and
; is used for debug purpose only

[Unit]
Description=DLT worker
After=network.target

[Service]
ExecReload=/bin/kill $MAINPID
KillMode=process
Restart=on-failure

Environment="MIX_ENV=prod"
; Path to the home directory of the user running the dlt-worker service.
Environment="HOME=/root"

; Path to the folder containing the dlt-worker installation.
WorkingDirectory=/var/dlt-worker
; Path to the Mix binary.
ExecStart=/usr/bin/elixir --name ${NODE_NAME}@${IP} --cookie ${COOKIE} -S mix run --no-halt

[Install]
WantedBy=multi-user.target