defmodule DLTWorker do
  @moduledoc """
  """

  defdelegate run(server \\ __MODULE__, params, manager), to: DLTWorker.Server

  defdelegate stop(server \\ __MODULE__), to: DLTWorker.Server
end
