defmodule DLTWorker.Target do
  @type request_result :: {:ok, map()} | {:error, any()}
  @callback call(url :: String.t()) :: [request_result()]

  def call(url) do
    Application.get_env(:dlt_worker, :target, DLTWorker.Target.HTTP).call(url)
  end
end
