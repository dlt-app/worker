defmodule DLTWorker.Server do
  @moduledoc """
  A worker server that is sending HTTP requests to a target HTTP endpoints.
  Multiple requests are sent asynchronously in bunches per second.
  """

  use GenServer

  require Logger

  defmodule State do
    @moduledoc """
    A struct to keep DLTWorker.Server state
    """

    defstruct status: :idle,
              duration: 0,
              requests: [],
              rps: nil,
              manager: nil,
              tasks: []
  end

  @type server :: atom() | pid() | {atom(), any()} | {:via, atom(), any()}
  @type request :: %{
          required(:url) => String.t(),
          required(:method) => String.t(),
          optional(:headers) => String.t(),
          optional(:body) => String.t()
        }
  @type run_params :: %{requests: [request()], duration: integer(), rps: integer()}

  @doc """
  Run the server to make `run_params.rps` asynchronous requests per second to
  `run_params.target` for `run_params.duration` seconds.
  Every second a new bunch of `run_params.rps` requests are made to
  the `run_params.target` asynchronously, even if previous requests are unfinished.
  This can lead to timeouts and errors on the target endpoint, finding which is one
  of the purposes of running the server.
  If the server is busy, it cannot be started again and `{:error, :already_started}`
  error will be returned.

  Every second the server will report to the `manager` (via Erlang's `send/2`)
  currently available stats in the form of `{:result, node(), results}`. The last
  report (when `run_params.duration` is reached) will have `:result_last` instead
  of `:result` to distinguish the message.
  No message is sent if no requests were fulfilled in past second.

  Upon completion, server state will be reset, it's status will become `:idle`,
  and the function can be called again.
  """
  @spec run(server(), run_params(), pid()) :: :ok | {:error, :already_running}
  def run(server \\ __MODULE__, run_params, manager) do
    GenServer.call(server, {:run, run_params, manager})
  end

  @doc """
  Stop running server cancelling all currently running requests and cleaning up
  it's state. If the server is not running, nothing will happen.
  """
  @spec stop(server()) :: :ok
  def stop(server \\ __MODULE__) do
    GenServer.call(server, :stop)
  end

  def start_link(args) do
    GenServer.start_link(__MODULE__, nil, args)
  end

  @impl GenServer
  def init(_args) do
    {:ok, %State{}}
  end

  @impl GenServer
  def handle_call({:run, _params, _pid}, _from, %State{status: :running} = state) do
    {:reply, {:error, :already_running}, state}
  end

  @impl GenServer
  def handle_call({:run, params, pid}, _from, %State{status: :idle} = state) do
    send(self(), {:tick, params.duration})

    {:reply, :ok,
     %State{
       state
       | duration: params.duration,
         requests: params.requests,
         rps: params.rps,
         manager: pid,
         status: :running
     }}
  end

  def handle_call(:stop, _from, state) do
    shutdown_tasks(state.tasks)
    {:reply, :ok, %State{}}
  end

  @impl GenServer
  def handle_info(
        {:tick, seconds_left},
        %State{requests: requests, rps: rps, manager: manager} = state
      ) do
    is_first_tick? = state.duration == seconds_left
    is_last_tick? = seconds_left == 0

    # Schedule next tick asap to keep up with time going on
    unless is_last_tick? do
      Process.send_after(self(), {:tick, seconds_left - 1}, 1000)
    end

    unfinished =
      if is_first_tick? do
        []
      else
        %{"results" => results, "unfinished" => unfinished} = parse_results(state.tasks)
        send(manager, {:result, state.duration - seconds_left, node(), results})
        unfinished
      end

    if is_last_tick? do
      shutdown_tasks(unfinished)
      {:noreply, %State{}}
    else
      tasks =
        Enum.map(1..rps, fn _i ->
          task =
            Task.async(fn ->
              Application.get_env(:dlt_worker, :target).call(requests)
            end)

          %{task: task, results: []}
        end)

      {:noreply, %State{state | tasks: unfinished ++ tasks}}
    end
  end

  def handle_info({ref, results}, state) do
    Process.demonitor(ref, [:flush])

    new_tasks_list =
      Enum.map(state.tasks, fn task ->
        if task.task.ref == ref do
          %{task | results: results}
        else
          task
        end
      end)

    {:noreply, %State{state | tasks: new_tasks_list}}
  end

  def handle_info({:DOWN, ref, _, _, _} = unexpected_down, state) do
    Logger.error("Unexpected DOWN message: #{inspect(unexpected_down)}")
    new_tasks_list = Enum.reject(state.tasks, &(&1.ref == ref))
    {:noreply, %State{state | tasks: new_tasks_list}}
  end

  defp parse_results(tasks) do
    data =
      Enum.reduce(tasks, %{"results" => [], "unfinished" => []}, fn
        %{results: []} = task, acc ->
          %{acc | "unfinished" => [task | acc["unfinished"]]}

        %{results: results}, acc ->
          %{acc | "results" => Enum.concat(acc["results"], results)}
      end)

    results_computed =
      data["results"]
      |> Enum.reduce(%{}, fn %{result: result, time: time}, acc ->
        Map.update(acc, result, [time], fn timings -> [time | timings] end)
      end)
      |> Enum.map(fn {result, timings} ->
        count = length(timings)
        mean = Enum.sum(timings) / count
        {result, %{count: count, mean: mean}}
      end)
      |> Enum.into(%{})

    Map.put(data, "results", results_computed)
  end

  defp shutdown_tasks(tasks) do
    Enum.each(tasks, fn task ->
      Task.shutdown(task.task, :brutal_kill)
      Process.demonitor(task.task.ref, [:flush])
    end)
  end
end
