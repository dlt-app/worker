defmodule DLTWorker.Target.HTTP do
  @behaviour DLTWorker.Target

  @impl DLTWorker.Target
  def call(requests) do
    Enum.map(requests, fn request0 ->
      request =
        Map.update(request0, :headers, [], fn headers ->
          if headers != nil do
            headers
            |> String.split("\n")
            |> Enum.reduce(%{}, fn kv, acc ->
              [key, value] =
                kv
                |> String.split(":")
                |> Enum.map(&String.trim/1)

              Map.put(acc, key, value)
            end)
            |> Enum.into([])
          else
            []
          end
        end)

      {time, result0} = :timer.tc(&perform_request/1, [request])

      result =
        case result0 do
          {:ok, %{status: status}} ->
            status

          {:error, error} ->
            error
            |> inspect()
            |> String.trim_leading(":")
        end

      %{time: time, result: result}
    end)
  end

  defp perform_request(%{method: "get"} = request) do
    Tesla.get(request.url, headers: request.headers)
  end

  defp perform_request(%{method: "post"} = request) do
    Tesla.post(request.url, request.body, headers: request.headers)
  end
end
