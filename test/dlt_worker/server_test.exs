defmodule DLTWorker.ServerTest do
  use ExUnit.Case, async: true

  import Mox

  alias DLTWorker.Server

  @response_codes [200, 302, 404, 500]

  @request_ok %{
    url: "http://site.com/ok",
    method: :post,
    headers: "header1: value1",
    body: "key1=val1"
  }
  @request_err %{url: "http://site.com/error", method: :get}
  @requests [@request_ok]

  defp result_for_url(url) do
    if String.contains?(url, "ok") do
      Enum.random(@response_codes)
    else
      "econnrefused"
    end
  end

  setup context do
    {:ok, server} = start_supervised({DLTWorker.Server, name: context.test})

    stub(DLTWorker.Target.Mock, :call, fn requests ->
      Enum.map(requests, fn %{url: url} ->
        rnd = Enum.random(1..50)
        Process.sleep(rnd)

        %{result: result_for_url(url), time: rnd}
      end)
    end)

    allow(DLTWorker.Target.Mock, self(), server)

    %{server: server}
  end

  describe "run" do
    test "can't run if already running", %{server: server} do
      rps = :rand.uniform(100)
      duration = 2

      params = %{
        requests: @requests,
        rps: rps,
        duration: duration
      }

      assert Server.run(server, params, self()) == :ok
      assert Server.run(server, params, self()) == {:error, :already_running}
    end

    test "can run after previous run finished", %{server: server} do
      rps = :rand.uniform(100)
      duration = 2

      params = %{
        requests: @requests,
        rps: rps,
        duration: duration
      }

      assert Server.run(server, params, self()) == :ok
      assert_receive({:result, ^duration, _, _}, 2500)
      assert Server.run(server, params, self()) == :ok
    end

    test "handles error responses", %{server: server} do
      rps = :rand.uniform(100)
      duration = 1

      params = %{
        requests: [@request_err],
        rps: rps,
        duration: duration
      }

      assert Server.run(server, params, self()) == :ok
      assert_receive({:result, ^duration, _node, results}, 1500)

      assert match?(%{"econnrefused" => %{count: _, mean: _}}, results)
    end

    test "runs rps*duration requests", %{server: server} do
      rps = :rand.uniform(100)
      duration = 2

      params = %{
        requests: @requests,
        rps: rps,
        duration: duration
      }

      expect(DLTWorker.Target.Mock, :call, rps * duration, fn requests ->
        Enum.map(requests, &%{result: result_for_url(&1.url), time: 42})
      end)

      Server.run(server, params, self())

      # wait to complete
      assert_receive({:result, ^duration, _, _}, 2500)
    end

    test "reports stats every second", %{server: server} do
      rps = :rand.uniform(100)
      duration = 2

      params = %{
        requests: @requests,
        rps: rps,
        duration: duration
      }

      Server.run(server, params, self())

      Enum.each(1..(duration - 1), fn _i ->
        assert_receive({:result, _second, _node, _results}, 1500)
      end)

      assert_receive({:result, ^duration, _node, _result}, 1500)
    end

    test "reports correct stats", %{server: server} do
      rps = :rand.uniform(100)
      duration = 3

      params = %{
        requests: @requests,
        rps: rps,
        duration: duration
      }

      Server.run(server, params, self())

      Enum.each(1..duration, fn i ->
        assert_receive({:result, second, _node, results}, 1500)
        assert second == i

        Enum.each(results, fn {code, data} ->
          assert code in @response_codes
          assert %{count: count, mean: _mean} = data
          assert count <= rps
        end)
      end)
    end

    test "Can handle multiple requests", %{server: server} do
      rps = 7
      duration = 3

      params = %{
        requests: [@request_ok, @request_err, @request_ok],
        rps: rps,
        duration: duration
      }

      Server.run(server, params, self())

      Enum.each(1..duration, fn i ->
        assert_receive({:result, second, _node, results}, 1500)
        assert second == i

        Enum.each(results, fn {code, data} ->
          assert code in ["econnrefused" | @response_codes]
          assert %{count: count, mean: _mean} = data
          assert count <= rps
        end)
      end)
    end
  end
end
